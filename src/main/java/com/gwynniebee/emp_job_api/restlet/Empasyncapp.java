/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.emp_job_api.restlet;

import javax.servlet.ServletException;

import com.gwynniebee.async.job.helper.AsyncJobRestletServlet;
import com.gwynniebee.emp_job_api.config.Config;
import com.gwynniebee.emp_job_api.restlet.resources.Empjob;

/**
 * Barcode file upload service routing and controlling resources.
 * @author skishore
 */

public class Empasyncapp extends AsyncJobRestletServlet {
    private static final int NUM_THREADS = 3;

    /**
     * COnstructor.
     */
    public Empasyncapp() {
        super(NUM_THREADS, "", Empjob.class);
    }

    @Override
    public void init() throws ServletException {
        Config.init();
        super.init();
    }

    @Override
    public void destroy() {
        super.destroy();
    }

}
