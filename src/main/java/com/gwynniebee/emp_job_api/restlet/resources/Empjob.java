/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.emp_job_api.restlet.resources;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.job.api.AsyncJobRequestMap;
import com.gwynniebee.async.job.api.AsyncJobStatus;
import com.gwynniebee.async.job.helper.AsyncJobResourceBase;
import com.gwynniebee.async.utils.JodaDateTimeSerializer;
import com.gwynniebee.emp_job_api.config.Config;

/**
 * Empjob class. To change this resource rename the class to the required class.
 * @author skishore
 */
public class Empjob extends AsyncJobResourceBase {

    private static final Logger LOG = LoggerFactory.getLogger(Empjob.class);

    /**
     * Constructor.
     */
    public Empjob() {
        super(AsyncJobRequestMap.class);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.async.job.helper.AsyncJobResourceBase#getJobName()
     */
    @Override
    public String getJobName() {
        return "empattendanceupdate";
    }

    @Override
    public String getJobType() {
        return "empattendanceupdate";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.async.job.helper.AsyncJobResourceBase#
     * estimateCompletionSeconds()
     */
    @Override
    protected int estimateCompletionSeconds() {
        return Config.TIME;
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.async.job.helper.AsyncJobResourceBase#executeBody()
     */
    @Override
    protected AsyncJobStatus<?> executeBody() throws Exception {
        // final AsyncJobRequestMap request = (AsyncJobRequestMap)
        // this.getJobRequest();
        // final LinkedHashMap<String, Object> jobData = request.getJobData();
        Map<String, Object> result = new HashMap<String, Object>();
        Date start = new Date();
        result.put("message", result.get("name") + ": Greetings from " + getJobType() + "." + getJobName() + " on "
                + JodaDateTimeSerializer.FORMATTER.print(new DateTime()));
        LOG.debug("EXECUTEEEEEEEEEEEEEEEEEEEEEEEEEEED");
        LOG.debug(Config.getRestUrl());
        ClientResource resource = new ClientResource(Config.getRestUrl());
        Representation res = resource.put(null);
        LOG.debug("ATTENDANCE UPDATE" + res.toString());
        result.put("getresponse", res.toString());
        Date end = new Date();
        String subject = getJobType() + "." + getJobName() + " has completed";
        String body =
                getJobType() + "." + getJobName() + " completed in " + Long.toString(end.getTime() - start.getTime()) + " milliseconds";

        return new AsyncJobStatus<Map<String, Object>>(result, subject, body);
    }

    /**
     * @return status
     * @throws Exception excpiton
     */
    public AsyncJobStatus<?> executeBodyPublic() throws Exception {
        return executeBody();
    }

}
