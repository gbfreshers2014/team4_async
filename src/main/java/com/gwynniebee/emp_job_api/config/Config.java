/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.emp_job_api.config;

import java.io.IOException;
import java.util.Properties;

import com.gwynniebee.iohelpers.IOGBUtils;

/**
 * BASE URL.
 * @author skishore
 */
public final class Config {
    private static String url = "http://hr-dev.gwynniebee.com:8080/employee_REST_API-v1/employee-portal/attendancetask.json";
    public static final int TIME = 10;
    /**
     * Constructor.
     */
    private Config() {

    }

    /**
     * URL Initailizer.
     */
    public static void init() {
        Properties prop = new Properties();
        prop = IOGBUtils.getPropertiesFromResource("/emp_async_prop.properties");
        url = prop.getProperty("URL");
    }

    /**
     * @return string
     * @throws IOException exception
     */
    public static String getRestUrl() throws IOException {
        return url;
    }
}
