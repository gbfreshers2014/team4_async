#! /usr/bin/env python

import os
import re
import sys
import tempfile
from getopt import getopt, GetoptError

gb_home = "/home/gb"

def usage():
    """Prints out the usage"""
    print """
Usage: %s -c <property-file>

-c    rest template configuration file
""" % (sys.argv[0])

def get_options():
    """Parses command line options.

@return: **options
"""
    # Required options.
    required = ["c"]

    # Get command line options.
    try:
        option_string = ''.join(map(lambda x: x + ":", required))
        options, [] = getopt(sys.argv[1:], option_string)
    except GetoptError:
        usage()
        exit(1)

    # Convert options to dict and validate.
    result = dict()
    for option in options:
        (opt, value) = option
        opt = opt[1:]
        if (opt in required):
            result[opt] = value
        else:
            usage()
            exit(1)

    # Verify that all mandatory parameters are provided.
    for option in required:
        if not result.has_key(option):
            usage()
            exit(1)

    return result

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "" and line[0] != "#":
            [key, value] = line.split("=")
            result[key] = value
    return result

def combine_war(war_file):
    """Builds a new war with configuration"""
    # Copy configuration file.
    tmpdir = tempfile.mkdtemp()
    os.mkdir(tmpdir + "/WEB-INF")
    classdir = tmpdir + "/WEB-INF/classes"
    os.mkdir(classdir)
    command = "cp %s/conf/emp_async/emp_async_prop.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/emp_async_prop.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "cp %s/conf/emp_async/log4j.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/log4j.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    lib_dir = tmpdir + "/WEB-INF/lib"
    os.mkdir(lib_dir)

    os.system("rm -rf " + tmpdir)

def main():
    """Main function"""
    options = get_options()
    config = parse_config_file(options["c"])

    server = config["application.server"]
    port = config["application.server.http.port"]
    uri = config["application.deployment.tomcat.manager.url"]
    login = config["application.deployment.tomcat.manager.login"]
    password = config["application.deployment.tomcat.manager.password"]
    context = config["application.context.path"]
    job_register_url = config["async_registration_url"]
    job_unregister_url = config["async_unregister_url"]

    war_file = gb_home + "/lib/emp_async/emp_async*.war"
    tomcat_path = config["application.deployment.tomcat.app.dir"]

    combine_war(war_file)

    print "Installing war"
    request_uri = "http://%s:%s@%s:%s%s/deploy?path=%s&update=true" \
                    % (login, password, server, port, uri, context)
    curl_command = "OUTPUT=`/usr/bin/curl -s -w '%%{http_code}\n' -X PUT --upload-file %s '%s' 2>&1`;echo \"$OUTPUT\";if echo \"$OUTPUT\" | grep -q '^2[0-9][0-9]$';then if echo $OUTPUT | grep -q '^FAIL';then false;else true;fi;else false;fi" \
                    % (war_file, request_uri)
    print "Executing: " + curl_command
    result = os.system(curl_command)
    if result != 0:
        print "Error: " + curl_command + " failed"
        sys.exit(1)
    os.system("curl -v -X DELETE '" + job_unregister_url + "'");
    os.system("curl -v -X POST -H 'Content-Type: application/json' '" + job_register_url + \
                    "' -d @/home/gb/conf/emp_async/schedule.json");
    

    sys.exit(0)

if __name__ == "__main__":
    main()
